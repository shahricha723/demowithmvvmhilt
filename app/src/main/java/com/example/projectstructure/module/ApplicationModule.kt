package com.example.projectstructure.module

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import com.example.projectstructure.BuildConfig
import com.example.projectstructure.utils.network.ApiConstant.Companion.BASE_URL
import com.example.projectstructure.utils.network.BaseDataSource
import com.example.projectstructure.utils.network.NetworkService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSession

@Module
@InstallIn(SingletonComponent ::class)
class ApplicationModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application.applicationContext
    }

	@Provides
	fun provideConnectivityManager(
		@ApplicationContext context: Context
	) = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

	@Provides
    @Singleton
    fun getBaseResponse(context: Context): BaseDataSource {
        return BaseDataSource(context)
    }

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()


    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson,  context: Context): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient(context))
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun provideNetworkService(retrofit: Retrofit): NetworkService =
        retrofit.create(NetworkService::class.java)

    @Provides
    fun okHttpClient(context: Context): OkHttpClient {
        val levelType: HttpLoggingInterceptor.Level = if (BuildConfig.DEBUG)
            HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

        val logging = HttpLoggingInterceptor()
        logging.setLevel(levelType)

        val httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
        httpClient.hostnameVerifier(object : HostnameVerifier {
            override fun verify(hostname: String?, session: SSLSession?): Boolean {
                return true
            }
        })
        httpClient.cache(null)

        httpClient.addInterceptor(Interceptor { chain ->
            val original: Request = chain.request()

            val requestBuilder = original.newBuilder()

            requestBuilder.addHeader("Cache-control", "no-cache")
            val request = requestBuilder.build()

            chain.proceed(request)
        })
        httpClient.callTimeout(2, TimeUnit.MINUTES)
        httpClient.connectTimeout(1, TimeUnit.MINUTES)
        httpClient.readTimeout(1, TimeUnit.MINUTES)
        httpClient.writeTimeout(5, TimeUnit.MINUTES)
        httpClient.addInterceptor(logging)

        val client = httpClient.build()
        return client

    }
}