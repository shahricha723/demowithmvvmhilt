package com.example.projectstructure

import android.app.Application
import android.content.Context
import androidx.lifecycle.LifecycleObserver
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ProjectStructure : Application(), LifecycleObserver {

    override fun onCreate() {
        super.onCreate()
        INSTANCE =this
    }

    companion object {
        private var INSTANCE: Context? = null
        fun getContext() = INSTANCE!!
    }
}