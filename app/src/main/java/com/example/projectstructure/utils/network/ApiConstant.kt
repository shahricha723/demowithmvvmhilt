package com.example.projectstructure.utils.network


interface ApiConstant {

    companion object {
        var BASE_URL: String = "https://simplifiedcoding.net/demos/"

        const val WEB_SERVICE_SUPER_HEROES:String="marvel"
    }
}