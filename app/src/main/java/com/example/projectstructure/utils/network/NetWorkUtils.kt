package com.example.projectstructure.utils.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Singleton
object NetWorkUtils   {
	fun isNetworkConnected(
		@ApplicationContext context: Context
	): Boolean {
		val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE)
		as ConnectivityManager
		return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)
			capabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)==true
		} else {
			val networkInfo = cm.activeNetworkInfo
			networkInfo!!.isConnected
		}
	}

}