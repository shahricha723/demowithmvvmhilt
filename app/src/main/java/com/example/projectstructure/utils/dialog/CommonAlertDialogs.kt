package com.example.projectstructure.utils.dialog

import android.content.Context
import com.example.projectstructure.R

fun Context.showInternetDialog(
    isCancelAble: Boolean = false,
    positiveClick: (() -> Unit)? = null
) {
    val builder = android.app.AlertDialog.Builder(this)
    builder.setTitle(this.getString(R.string.project_structure))
    builder.setMessage(this.getString(R.string.network_connection_error))
    builder.setPositiveButton(this.getString(R.string.ok)) { _, _ ->
        positiveClick?.invoke()
    }


    // builder.setPositiveButton(this.getString(R.string.ok),  positiveClick?.invoke())
    builder.setCancelable(isCancelAble)
    val alertDialog = builder.create()
    alertDialog.show()
}
