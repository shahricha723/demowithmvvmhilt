package com.example.projectstructure.utils

import android.app.Activity
import android.app.Application
import com.example.projectstructure.R

/**
* validation status of all screen
**/
enum class ValidationStatus {
    // set validation status
    EMPTY_EMAIL,
    INVALID_EMAIL,
    EMPTY_PASSWORD
}

object Validation {
    //set validation toast message
    fun showError(activity: Application, validationStatus: ValidationStatus) {
        val message = getMessage(activity, validationStatus)
        if (message.isNotEmpty()) {
            activity.showToast(message)
        }
    }

    // set message string for validation status
    fun getMessage(activity: Application, it: ValidationStatus): String {
        return when (it) {
            ValidationStatus.EMPTY_EMAIL -> activity.getString(R.string.please_enter_email)
            ValidationStatus.INVALID_EMAIL -> activity.getString(R.string.please_enter_valid_email)
            ValidationStatus.EMPTY_PASSWORD -> activity.getString(R.string.please_enter_password)
            else -> ""
        }
    }
}