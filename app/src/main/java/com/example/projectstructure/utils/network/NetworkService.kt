package com.example.projectstructure.utils.network

import com.example.projectstructure.data.remote.home.SuperHeroesResponse
import retrofit2.Response
import retrofit2.http.*


interface NetworkService {
    @GET(ApiConstant.WEB_SERVICE_SUPER_HEROES)
    suspend fun getSuperHeroes(): Response<SuperHeroesResponse>
}