package com.example.projectstructure.utils.network

import android.content.Context
import com.example.projectstructure.R
import com.example.projectstructure.data.remote.home.SuperHeroesResponse
import org.json.JSONObject
import retrofit2.Response


open class BaseDataSource(val context : Context) {
    open suspend fun <T> getResult(
        isGetProfile: Boolean = false,isSignin: Boolean = false,
        call: suspend () -> Response<T>
    ): Resource<T> {
        try {

            val response = call()
            when {
                response.code() == 403 -> {
                    return Resource.error("", code = response.code())
                }
                response.code() == 500 -> {
                    return Resource.error(
                        context.getString(R.string.server_temporarily_unavailable),
                        code = response.code()
                    )
                }
                response.code() == 503 -> {
                    return Resource.error(
                        context.getString(R.string.server_temporarily_unavailable),
                        code = response.code()
                    )
                }
                response.code() == 502 -> {
                    return Resource.error("", code = response.code())
                }
                response.code() == 400-> {
                    return Resource.error(context.getString(R.string.unauthorized), code = response.code())
                }
                response.code() == 401 -> {
                    return Resource.error(context.getString(R.string.your_session_is_expired), code = response.code())
                }
                response.body() != null -> {
                    //will be change as per the data class structure
                    val baseResponse = (response.body() as SuperHeroesResponse)
                    if (response.isSuccessful /*&& baseResponse.record == 200*/) {
                        response.body()?.let {
                            return Resource.success(it, context.getString(R.string.success))
                        }
                    } else {
                        val body = response.body()
                        if (body != null) {
                            return Resource.error(context.getString(R.string.something_went_wrong))
                        }
                    }
                    return error("Success => ${response.code()} ${response.message()}")
                }
                else -> {
                    return try {
                        val jObjError = JSONObject(response.errorBody()!!.string())
                        val msg = jObjError.getString(context.getString(R.string.message))
                        Resource.error(msg)
                    } catch (e: Exception) {
//                        coroutineScope {
//                            delay(200)
                        if(NetWorkUtils.isNetworkConnected(context)) {
                            Resource.error(context.getString(R.string.something_went_wrong))
                        }else{
                            Resource.error(context.getString(R.string.network_connection_error))
                        }
//                        }
                    }
                }
            }
        } catch (e: Exception) {
            return error("Error => ${e.message} ?: ${e}")
        }

    }

    private fun <T> error(message: String): Resource<T> {
        return Resource.error(message)
    }
}