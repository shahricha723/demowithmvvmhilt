package com.example.projectstructure.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.example.projectstructure.utils.dialog.CustomProgressDialog
import javax.inject.Inject


abstract class BaseActivity<T : ViewDataBinding, V : ViewModel> : AppCompatActivity() {

    abstract val layoutId: Int
    abstract val bindingVariable: Int

    @Inject
    lateinit var mViewModel: V
    lateinit var binding: T

    private var progress: CustomProgressDialog? = null

    abstract fun setupObservable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progress = CustomProgressDialog(this)
        performDataBinding()
    }

    private fun performDataBinding() {

        //set binding
        binding = DataBindingUtil.setContentView(this, layoutId)

        binding.setVariable(bindingVariable, mViewModel)
        binding.executePendingBindings()

        // set observer to activity
        setupObservable()
    }

    fun showProgress() {
        if (!this.isFinishing) {
            progress?.show()
        }
    }

    fun hideProgress() {
        if (!this.isFinishing && progress?.isShowing == true) {
            progress?.dismiss()
        }
    }

    fun isProgressShowing(): Boolean {
        return progress?.isShowing == true
    }
}