package com.example.projectstructure.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import com.example.projectstructure.utils.network.BaseDataSource
import java.lang.ref.WeakReference
import javax.inject.Inject

open class BaseAndroidViewModel<N>(application: Application) :
	AndroidViewModel(application)

 {

    lateinit var mNavigator: WeakReference<N>

    fun getNavigator(): N? {
        return mNavigator.get()
    }


    fun setNavigator(navigator: N) {
        mNavigator = WeakReference(navigator)
    }

    @Inject
    lateinit var baseDataSource: BaseDataSource


}