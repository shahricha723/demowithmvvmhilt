package com.example.projectstructure.activity.home

import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.projectstructure.BR
import com.example.projectstructure.R
import com.example.projectstructure.base.BaseActivity
import com.example.projectstructure.data.remote.home.SuperHeroesResponse
import com.example.projectstructure.databinding.ActivityHomeBinding
import com.example.projectstructure.utils.dialog.showInternetDialog
import com.example.projectstructure.utils.network.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : BaseActivity<ActivityHomeBinding, HomeViewModel>(), HomeNavigator {

    override val layoutId: Int
        get() = R.layout.activity_home
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.setNavigator(this)
        binding.viewModel = mViewModel

        apiCallForSuperHeroesDetail()
    }

    private fun apiCallForSuperHeroesDetail() {
        mViewModel.getSuperHeroesDetail()
    }

    override fun setupObservable() {
        mViewModel.getSuperHeroesDataObservable().observe(this) {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    it.data?.let {
                        hideProgress()
                        getData(it)
                    }
                }
                Resource.Status.ERROR -> {
                    hideProgress()
                    Log.e("error", "Loading =>  ${it.message}")
                }

                Resource.Status.LOADING -> {
                    showProgress()
                    Log.e("onResult", "Loading =>  ${it.message}")
                }

                Resource.Status.NO_INTERNET_CONNECTION -> {
                    hideProgress()
                    showInternetDialog(false)
                }
                else -> {

                }
            }
        }
    }

    private fun getData(data: SuperHeroesResponse){
        binding.rvSuperHeroes.layoutManager = LinearLayoutManager(this)
        binding.rvSuperHeroes.adapter = SuperHeroesListAdapter(this@HomeActivity, data)
    }
}