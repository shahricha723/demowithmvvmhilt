package com.example.projectstructure.activity.signIn

interface SignInNavigator {
  // all click events of sign in screen
  fun signIn()
}