package com.example.projectstructure.activity.home

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.projectstructure.data.remote.home.SuperHeroesResponseItem
import com.example.projectstructure.databinding.ItemSuperHeroesBinding

class SuperHeroesListAdapter(
    private val context: Context,
    private var list: ArrayList<SuperHeroesResponseItem>
) : RecyclerView.Adapter<SuperHeroesListAdapter.ButtonViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ButtonViewHolder(
            ItemSuperHeroesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder:ButtonViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemCount() = list.size

    inner class ButtonViewHolder(binding: ItemSuperHeroesBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val mBinding: ItemSuperHeroesBinding = binding
        fun onBind(position: Int) {
            val data: SuperHeroesResponseItem = list[position]
            mBinding.dataModel = data
            mBinding.executePendingBindings()
        }
    }

}