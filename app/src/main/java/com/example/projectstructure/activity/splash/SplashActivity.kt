package com.example.projectstructure.activity.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.example.projectstructure.BR
import com.example.projectstructure.R
import com.example.projectstructure.activity.home.HomeActivity
import com.example.projectstructure.activity.signIn.SignInActivity
import com.example.projectstructure.base.BaseActivity
import com.example.projectstructure.databinding.ActivitySplashBinding
import com.example.projectstructure.utils.startNewActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>(), SplashNavigator {

    override val layoutId: Int
        get() = R.layout.activity_splash
    override val bindingVariable: Int
        get() = BR.viewModel

    private var handler = Handler(Looper.getMainLooper())
    private var timeOut: Long = 2000

    private var runnable = Runnable {
        redirectionActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.setNavigator(this)
        binding.viewModel = mViewModel
        handler.postDelayed(runnable, timeOut)
    }

    private fun redirectionActivity() {
        startNewActivity(SignInActivity::class.java,true)
    }

    override fun setupObservable() {

    }
}