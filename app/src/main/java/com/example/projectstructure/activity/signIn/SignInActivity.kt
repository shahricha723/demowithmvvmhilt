package com.example.projectstructure.activity.signIn

import android.os.Bundle
import com.example.projectstructure.BR
import com.example.projectstructure.R
import com.example.projectstructure.activity.home.HomeActivity
import com.example.projectstructure.base.BaseActivity
import com.example.projectstructure.databinding.ActivitySignInBinding
import com.example.projectstructure.utils.startNewActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignInActivity : BaseActivity<ActivitySignInBinding, SignInViewModel>(), SignInNavigator {

    override val layoutId: Int
        get() = R.layout.activity_sign_in
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.setNavigator(this)
        binding.viewModel = mViewModel
    }

    override fun setupObservable() {
        mViewModel.getEmailErrorObservable().observe(this) {
            binding.edtEmail.error = it
            binding.edtEmail.requestFocus()
        }

        mViewModel.getPasswordErrorObservable().observe(this) {
            binding.edtPass.error = it
            binding.edtPass.requestFocus()
        }
    }

    override fun signIn() {
        startNewActivity(HomeActivity::class.java,true)
    }
}