package com.example.projectstructure.activity.home

import android.app.Activity
import android.app.Application
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.projectstructure.base.BaseAndroidViewModel
import com.example.projectstructure.base.BaseViewModel
import com.example.projectstructure.data.remote.home.SuperHeroesResponse
import com.example.projectstructure.utils.network.NetWorkUtils
import com.example.projectstructure.utils.network.NetworkService
import com.example.projectstructure.utils.network.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val networkService:
                                        NetworkService,
                                        private  val application:
                                        Application) :
	BaseAndroidViewModel<HomeNavigator>(application) {

    fun getSuperHeroesDetail() {
        if (NetWorkUtils.isNetworkConnected(application)) {
                var response: Response<SuperHeroesResponse>? = null
                viewModelScope.launch {
                    superHeroesDataResponseObservable.value = Resource.loading(null)
                    withContext(Dispatchers.IO) {
                        try {
                            response = networkService.getSuperHeroes()
                        } catch (e: Exception) {
                            viewModelScope.launch {
                                withContext(Dispatchers.Main) {
                                    superHeroesDataResponseObservable.value =
                                        Resource.noInternetConnection(null)
                                }

                            }

                        }
                    }
                    withContext(Dispatchers.Main) {
                        if (response != null) {
                            response.run {
                                superHeroesDataResponseObservable.value =
                                    baseDataSource.getResult(isSignin = true){ this!! }
                            }
                        } else {

                        }
                    }
                }
        } else {
            viewModelScope.launch {
                withContext(Dispatchers.Main) {
                    superHeroesDataResponseObservable.value = Resource.noInternetConnection(null)
                }
            }
        }
    }


    private val superHeroesDataResponseObservable: MutableLiveData<Resource<SuperHeroesResponse>> =
        MutableLiveData()

    fun getSuperHeroesDataObservable(): LiveData<Resource<SuperHeroesResponse>> {
        return superHeroesDataResponseObservable
    }
}