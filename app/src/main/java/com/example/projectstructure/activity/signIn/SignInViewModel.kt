package com.example.projectstructure.activity.signIn

import android.app.Activity
import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.projectstructure.R
import com.example.projectstructure.base.BaseAndroidViewModel
import com.example.projectstructure.base.BaseViewModel
import com.example.projectstructure.utils.*
import javax.inject.Inject

class SignInViewModel@Inject constructor( private  val application: Application
) : BaseAndroidViewModel<SignInNavigator>(application) {

    var email = ""
    var password = ""

    //set observer for email
    private val emailErrorObservable: MutableLiveData<String> =
        MutableLiveData()

    fun getEmailErrorObservable(): LiveData<String> {
        return emailErrorObservable
    }

    //set observer for password
    private val passwordErrorObservable: MutableLiveData<String> =
        MutableLiveData()

    fun getPasswordErrorObservable(): LiveData<String> {
        return passwordErrorObservable
    }

    //sign in click event
    fun signIn() {
        if (isValidDetail()) {
            if (email == "test@gmail.com" && password == "12345678"){
                getNavigator()?.signIn()
            }else{
	            application.showToast(application.resources.getString(R.string.please_enter_valid_username_or_password))
            }
        }
    }

    private fun isValidDetail(): Boolean {
        if (email.isEmpty() || !email.isEmailValid()) {
            if (email.isEmpty()) {
                emailErrorObservable.postValue(
                    Validation.getMessage(
	                    application,
                        ValidationStatus.EMPTY_EMAIL
                    )
                )
            } else if (!email.isEmailValid()) {
                emailErrorObservable.postValue(
                    Validation.getMessage(
	                    application,
                        ValidationStatus.INVALID_EMAIL
                    )
                )
            }
            return false
        }else if (password.isEmpty()) {
            passwordErrorObservable.postValue(
                Validation.getMessage(
	                application,
                    ValidationStatus.EMPTY_PASSWORD
                )
            )
            return false
        } else {
            return true
        }
    }

}